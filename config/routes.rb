Rails.application.routes.draw do

 root 'recipyes#index'

 devise_for :users, :path => 'u' 

 get 'users/index/:id', to: "users#index", as:"user_recipes"
 get 'users/list', to: "users#list_users", as: "list_users"
 get 'users/list/banned', to: "users#list_banned_users", as: "list_banned_users"
 get 'users/list/recipes', to: "users#list_of_recipes", as: "list_of_recipes"
 get 'users/list/comments', to: "users#list_of_comments", as: "list_of_comments"

 resources :categories, except:[:new,:show]
 resources :relationships, only:[:create]
 get 'delete/:id', to: 'relationships#delete', as:'unfollow' 
 
 resources :users, only: [:show, :destroy] do 
 	member do
 	 patch 'banned'
 	end
 	end
 
 resources :recipyes do
 resources :comments, only: [:create, :destroy]
  
  collection do
   get "search" 
  end

  member do
   put 'like', to: 'recipyes#upvote'
	 end
 end

 get '*path' => redirect('/')
end
