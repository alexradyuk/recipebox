require 'rails_helper'

RSpec.describe User, type: :model do
  let(:kate) {User.create(name:'Kate', email:'kate@gmail.com', password:789123, password_confirmation:789123)}
  let(:dem) {User.create(name:'Dem34', email:'dem@gmail.com', password:789123, password_confirmation:789123)}
  let(:alex) {User.create(name:'Alex', email:'alex@gmail.com', password:789123, password_confirmation:789123)}
  
  it "It should create instance of Relationship" do

  	relationship = Relationship.create(follower_id: kate.id, following_id: dem.id)
  	expect(kate.follower_relationships).to include(relationship)
    expect(dem.following_relationships).to include(relationship)
  end

  it "It should create many followers and following" do

  	Relationship.create(follower_id: kate.id, following_id: dem.id)
  	Relationship.create(follower_id: alex.id, following_id: dem.id)
  	Relationship.create(follower_id: kate.id, following_id: alex.id)
  	
  	expect(kate.following).to include(alex, dem)
    expect(dem.followers).to include(kate, alex)   
  end


end
