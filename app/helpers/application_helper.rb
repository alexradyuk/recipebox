module ApplicationHelper

	def to_link(text)
		(text).gsub! /\s?(https?:\/\/[\S]+)\s?/,'<a href="\1">\1</a>'	
		text.html_safe
	end
 
 def follow?(follower_user, user)
  value = Relationship.where(follower_id:"#{follower_user.id}", following_id:"#{user.id}").count
  return false if value == 0
  return true if value !=0
 end

end
