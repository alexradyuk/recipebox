class Recipe < ActiveRecord::Base
 
 searchkick
 
 has_attached_file :image, styles: { medium: "350x350>"}, default_url: "missing.png"
 validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/

 validates :title, presence:true, uniqueness: true, length:{maximum:51}
 validates :description, presence:true, length:{maximum:351}

 belongs_to :user
 belongs_to :category
 
 has_many :comments, dependent: :destroy
 has_many :upvotes, dependent: :destroy
 has_many :upvoted_users, through: :upvotes, source: :user
 has_many :ingredients, dependent: :destroy
 has_many :directions, dependent: :destroy
 
 accepts_nested_attributes_for :ingredients, reject_if: proc {|attribute| attribute['name'].blank?},
 															               allow_destroy: true
 accepts_nested_attributes_for :directions, reject_if: proc {|attribute| attribute['step'].blank?}, 
 																						 allow_destroy: true

end
