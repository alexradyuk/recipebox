class Comment < ActiveRecord::Base

	belongs_to :user
	belongs_to :recipe

	validates :body, presence: true
	validates :body, length:{maximum:450}
 default_scope -> {order('created_at DESC')}

end
