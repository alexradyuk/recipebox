class RecipyesController < ApplicationController

 before_action :recipe_find, only:[:show, :edit, :update, :destroy, :upvote]
 before_action :authenticate_user!, except: [:index, :show, :search, :upvote]
 before_action :correct_user, only: [:edit, :update, :destroy]
 before_action :categories, only: [:new, :create, :edit, :update]
 
 def index
  if params[:category]
   @category = Category.find_by(name: params[:category])
   @recipies = Recipe.where('category_id = ?', @category).paginate(page: params[:page], per_page:6).order('created_at DESC')
  else
   @recipies = Recipe.all.paginate(page: params[:page], per_page:6).order('created_at DESC')
  end
 end

 def new
  @recipe = Recipe.new
 end

 def create
  @user = current_user
  @recipe = @user.recipes.build(recipe_params)
  if @recipe.save
    flash[:success] = "#{@recipe.title.capitalize} was created"
   	redirect_to recipye_path(@recipe)
  else
 	  render :new
  end
 rescue Faraday::ConnectionFailed #Elasticksearch off
 redirect_to recipye_path(@recipe)
 end

 def show 
  @comment = @recipe.comments.build
 end

 def edit
 end

 def update
  if @recipe.update(recipe_params)
 	 flash[:success] = "Updated!"
 	 redirect_to recipye_path(@recipe)	
  else
 	 render :edit
  end
 rescue Faraday::ConnectionFailed #Elasticksearch off
  redirect_to recipye_path(@recipe)
 end

 def destroy
  @recipe.destroy
 rescue Faraday::ConnectionFailed #Elasticksearch off
  flash[:success] = "Destroyed!"
  redirect_to root_path
 end

 def search
  if params[:search].present?
	 @recipies = Recipe.search(params[:search]) 
  else
	 flash[:alert] = "Typing your key words"
   redirect_to :back
  end
 rescue Faraday::ConnectionFailed #Elasticksearch off
  render_500
 end

 def upvote
   respond_to do |format|
   format.html 
   format.js
  end
 end

 private
 
 def correct_user
  @recipe_compare = current_user.recipes.find_by(id: params[:id])
  if @recipe_compare.nil? && current_user.admin == false
	flash[:error] = "Permission denied!"
	redirect_to recipye_path(@recipe) 
  end 
 end

 def recipe_params
  params.require(:recipe).permit(:title, :description, :image, :category_id, ingredients_attributes:[:id, :name, :_destroy], directions_attributes: [:id, :step, :_destroy])
 end

 def recipe_find
  @recipe = Recipe.find(params[:id])
 rescue ActiveRecord::RecordNotFound
  render_404
 end

 def categories
  @categories = Category.all
 end

end
