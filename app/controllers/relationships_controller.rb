class RelationshipsController < ApplicationController
 
 def create
  @relationship = Relationship.create(params[:relationship].permit(:follower_id, :following_id))
   respond_to do |format|
   format.html {redirect_to :back}
   format.js 
  end
 rescue ActiveRecord::RecordNotUnique
 	 flash[:error] = "You have already done it!"
 	 redirect_to user_path(@user)
 end
 
 def delete
  @relationships = Relationship.where('follower_id = ? AND following_id = ?', current_user.id, params[:id]).destroy_all
  respond_to do |format|
   format.html {redirect_to user_path(params[:id])}
   format.js 
  end 
 end

end