class UsersController < ApplicationController

	before_action :user_find, only:[:index, :show, :banned, :destroy]
	before_action :user_params, only:[:banned]
  before_action :if_admin, except: [:index, :edit, :show]

 def index
	@recipies = @user.recipes.paginate(page: params[:page], per_page:6).order('created_at DESC')
 end

 def list_users
 	@users = User.where(admin:false).paginate(page: params[:page], per_page:10).order('created_at DESC')
 end
 
 def list_banned_users
 	@users = User.where(banned:true).paginate(page: params[:page], per_page:10).order('created_at DESC')
  render :list_users
 end
 
 def list_of_recipes
  @recipes = Recipe.all.paginate(page: params[:page], per_page:20).order('created_at DESC')
  render :list_users
 end
 
 def list_of_comments
  @comments = Comment.all.paginate(page: params[:page], per_page:10).order('created_at DESC')
  render :list_users
 end

 def banned
  @user.update_attributes(user_params) 
  flash[:success] = "User was unlocked!"  if ! @user.banned?
  flash[:error] = "User was locked!" if @user.banned?
  redirect_to :back
 end

 def destroy
 	@user.destroy 
 	flash[:error] = "#{@user.name.capitalize} was destroyed!"
 	redirect_to list_users_path
 end

 def edit
 end

 def show
  @relationship = Relationship.new
  @users = User.where(admin:false)
  @banned_users_count = User.where(banned:true).count
  @categories = Category.all
  @recipes = Recipe.all
  @comments = Comment.all
 end

 private

 def user_find
  @user = User.find(params[:id]) 
 rescue ActiveRecord::RecordNotFound
  render_404
 end

 def user_params
 	params.require(:user).permit(:banned)
 end

end
