class CommentsController < ApplicationController

 before_action :authenticate_user!
 before_action :set_recipe
 before_action :correct_user, only: [:destroy]
 
 def create
 	@comment = @recipe.comments.build(comment_params)
  @comment.user_id = current_user.id if current_user
   if @comment.save
    redirect_to recipye_path(@recipe)
   else
    render 'recipyes/show'
  end
 end

 def destroy
  @comment = @recipe.comments.find(params[:id])
  @comment.destroy
  respond_to do |format|
    format.html {redirect_to recipye_path(@recipe)}
    format.js {} 
  end
 rescue ActiveRecord::RecordNotFound
  render_404
 end

 private 

 def set_recipe
  @recipe = Recipe.find(params[:recipye_id])
 end

 def comment_params
 	params.require(:comment).permit(:body)
 end
 
 def correct_user
  @comment_compare = current_user.comments.find_by(id: params[:id])
  if @comment_compare.nil? && current_user.admin == false
   flash[:error] = "Permission denied!"
   redirect_to recipye_path(@recipe) 
  end 
 end

end
