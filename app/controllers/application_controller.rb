class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :banned?

  protected

  def configure_permitted_parameters
   devise_parameter_sanitizer.permit(:sign_up) { |u| u.permit(:name,:avatar, :email, :password) }
   devise_parameter_sanitizer.permit(:account_update) { |u| u.permit(:name, :avatar, :email, :password, :current_password) }
  end
  
 private

 def render_404
  render file:'public/404', status:404
 end

 def render_500
  render file:'public/500', status:500
 end

 def if_admin
  render_404 unless user_signed_in? && current_user.admin 
 end

  def banned?
    if current_user.present? && current_user.banned?
      sign_out current_user
      flash[:error] = "Account was locked, you need to contact with administrator"  
      redirect_to :back
   end 
  end

end
