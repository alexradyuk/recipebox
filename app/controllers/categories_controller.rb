class CategoriesController < ApplicationController

 before_action :if_admin
 before_action :find_category, except:[:index, :create]
 before_action :share_variables, except:[:destroy]

  def index
    @category = Category.new
  end

  def create
  @category = Category.new(params[:category].permit(:name))
   if @category.save
    flash[:success] = "Was added"
    redirect_to :back
   else
    render :index
  end
  end

  def edit
   render :index
  end

  def update
   if @category.update(params[:category].permit(:name))
    flash[:success] = "Updated"
    redirect_to categories_path
   else
    render :index
   end
  end

  def destroy
   @category.destroy
   flash[:success] = "'#{@category.name.capitalize}' was destroyed"
   redirect_to categories_path
  end

 private

  def share_variables
   @categories = Category.all.paginate(page: params[:page], per_page:10).order('created_at DESC')
  end

  def find_category
   @category = Category.find(params[:id])
  rescue ActiveRecord::RecordNotFound
   render_404
  end

end
