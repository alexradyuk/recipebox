FROM radyukrubyst/rails:pre-installed
MAINTAINER Alex Radyuk "alex.radzyuk@gmail.com"
RUN mkdir -p /recipebox
WORKDIR /recipebox
#ADD . /recipebox
ADD .env /recipebox
ADD unicorn_recipebox /etc/init.d/
ADD recipebox /etc/nginx/sites-available
RUN cd /etc/nginx/sites-enabled/ && ln -s /etc/nginx/sites-available/recipebox recipebox && \ 
    rm -rf default && useradd -m unicorn && chmod +x /etc/init.d/unicorn_recipebox
RUN /bin/bash -l -c "echo 'gem --no-ri --no-rdoc' > ~/.gemrc && echo 'daemon off;' >> /etc/nginx/nginx.conf"
RUN /bin/bash -l -c "cd /recipebox && rvm install 2.1.4 && rvm use 2.1.4 --default && \ 
    gem install bundler && bundle install --without development && \ 
    rake assets:precompile RAILS_ENV=production && rake db:migrate RAILS_ENV=production"
RUN mkdir -p shared/pids shared/sockets shared/log && chown -R unicorn:unicorn shared/ && chmod -R 777 /recipebox \
    apt-get clean && apt-get autoclean && apt-get autoremove
#Need: nginx virtual_host_config, unicorn bash script, file .env, last RUN install cron for 'whenever gem'
