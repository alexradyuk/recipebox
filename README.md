== README
 
The book of recipes, comfortable place for keeping your recipes,
and sharing them with your friends and relatives.

Steps for realization:

* Ruby version - 2.1.4;

* Rails version - 4.2.6;

* Using Gems: Devise, Searchkick, SimpleForm, Paperclip, Cocoon, Haml, Bootstrap, Dotenv..etc

* OS - Linux: Ubuntu Server;

* Services: Postgesql, 'Image Magic' for Paperclip's gem, and 'Elasticsearch' for Searchkick.
  
* Install and configuration: make `git clone 'repo' && cd 'repo' && bundle install`;

* You should to set environment variables in the .env file in root of your app directory for: (DB, ACTIONMAILER, SECRETKEY, ELASTICSEARCH);

* Setup connection with database; 

* Make `rake db:create && rake db:migrate`;

* Next step `rake searchkick:reindex CLASS=Recipe` or your search won't be working;

* To turn on Admin instead of usual user, should update boolean column Admin to the value 'true' for your defined user. With admin role you can manage all of the app resources.

* Done, try to start your app.